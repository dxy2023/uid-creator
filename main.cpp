#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QUuid>

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QWidget widget;
    QLineEdit edit(&widget);
    QPushButton button(QObject::tr("生成"), &widget);
    QHBoxLayout layout;
    layout.addWidget(&edit, 8);
    layout.addWidget(&button, 2);
    widget.setLayout(&layout);

    widget.connect(&button, &QPushButton::clicked, &widget, [ & ] {
        QUuid uid = QUuid::createUuid();
        edit.setText(uid.toString(QUuid::WithoutBraces));
    });

    widget.setMinimumSize(400, 60);
    widget.show();


    return app.exec();
}
